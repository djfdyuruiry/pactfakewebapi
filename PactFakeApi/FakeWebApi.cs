﻿using System;
using System.Collections.Generic;
using PactNet;
using PactNet.Mocks.MockHttpService;
using PactNet.Mocks.MockHttpService.Models;

namespace PactFakeApi
{
    public class FakeWebApi : IDisposable
    {
        public static readonly int MockServerPort = 1234;
        public static readonly string MockProviderServiceBaseUri = $"http://localhost:{MockServerPort}";

        public IPactBuilder PactBuilder { get; }
        public IMockProviderService MockProviderService { get; }
        public List<string> ValidSomethings { get; }

        public FakeWebApi()
        {
            ValidSomethings = new List<string> { "me", "you", "tester" };

            PactBuilder = new PactBuilder(); //Uses default directories. PactDir: ..\..\pacts and LogDir: ..\..\logs

            PactBuilder
                .ServiceConsumer("Consumer")
                .HasPactWith("Something API");

            MockProviderService = PactBuilder.MockService(MockServerPort); //Configure the http mock server
        }

        public void SetupFakeSomethingsEndpoint()
        {
            ValidSomethings.ForEach(s =>
            {
                MockProviderService
                    .UponReceiving($"A GET request to retrieve something with id '{s}'")
                    .With(new ProviderServiceRequest
                    {
                        Method = HttpVerb.Get,
                        Path = $"/somethings/{s}",
                        Headers = new Dictionary<string, string>
                        {
                            { "Accept", "application/json" }
                        }
                    })
                    .WillRespondWith(new ProviderServiceResponse
                    {
                        Status = 200,
                        Headers = new Dictionary<string, string>
                        {
                            { "Content-Type", "application/json; charset=utf-8" }
                        },
                        Body = new //NOTE: Note the case sensitivity here, the body will be serialised as per the casing defined
                        {
                            id = s,
                            firstName = "Totally",
                            lastName = "Awesome"
                        }
                    });
            });
        }

        public void Dispose()
        {
            PactBuilder.Build(); //NOTE: Will save the pact file once finished
        }
    }
}
