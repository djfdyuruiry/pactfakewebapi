﻿using System;

namespace PactFakeApi
{
    class Program
    {        
        static void Main(string[] args)
        {
            var fakeWebApi = new FakeWebApi();
            fakeWebApi.SetupFakeSomethingsEndpoint();
            
            Console.Write(FakeWebApi.MockProviderServiceBaseUri);

            Console.ReadKey();
        }
    }
}
